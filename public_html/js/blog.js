$ (function () {
    
    var APPLICATION_ID = "FD9F4EFC-597A-866C-FF01-BC608D2F1000",
        SECRET_KEY = "C18DC4A1-CB9A-FA3B-FF94-10C367248E00",
        VERSION = "v1";
    
    
     Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
    
    var postsCollection = Backendless.Persistence.of(Posts).find();
    
    console.log(postsCollection);
    
    var wrapper = {
        posts: postsCollection.data
    };
    
    Handlebars.registerHelper('format', function (time) {
        return moment(time).format("dddd, MMMM Do YYYY");
    });
    
    var blogScript = $("#blogs-template").html();
    var blogTemplate = Handlebars.compile(blogScript);
    var blogHTML = blogTemplate(wrapper);
    
    $('.main-container').html(blogHTML);
    
    
});

function Posts(args){
    args = args || {};
    this.title = args.title || "";
    this.contest = args.content || "";
    this.authorEmail = args.authorEmail || "";
}
 $(document).ready(function(){
    $('.collapsible').collapsible({
      accordion : false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
    });
  });
  